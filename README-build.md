Compiling MeeSays
=================

This project was created using Qt Creator 2.4.1 as installed by Nokia's
QtSdk-offline-linux-x86_64-v1.2.1.run. It should be fairly trivial to compile if
you use the same version of Qt Creator -- just click the button.

However, the project can also be compiled on the command line. You will still
need to download the SDK but you won't have to run the GUI. In particular, you
will need to get Nokia's Maemo Application Development and Debugging Environment
(MADDE) which is included the the QtSdk package.

Assuming that you have the MeeSays project checked out at `~/projects/meesays`
and the SDK at `~/opt/QtSDK`, you can use these commands to build MeeSays:

    mkdir -p ~/projects/meesays-build ; cd ~/projects/meesays-build
    cp -ar ../meesays/qtc_packaging/debian_harmattan/ debian
    ~/opt/QtSDK/Madde/bin/mad set harmattan_10.2011.34-1_rt1.2
    ~/opt/QtSDK/Madde/bin/mad qmake ../meesays/meesays.pro
    ~/opt/QtSDK/Madde/bin/mad dpkg-buildpackage -nc -uc -us

Afterwards you should have a Debian package called `meesays_<version>_armel.deb`
in `~/projects`.


Using docker
============

If you repackage your QtSDK to a tarball, this `Dockerfile` should give you a
nice little docker image that you can use to compile any Harmattan application:

    FROM debian:stretch
    RUN apt-get update \
            && apt-get -y install --no-install-recommends \
                    file \
                    libperl5.24 \
                    make \
                    perl-modules \
            && rm -rf /var/lib/apt/*
    ADD QtSdk-offline-linux-x86_64-v1.2.1.tar.gz /opt/

To get the tarball, install and extract the SDK to `/opt/QtSDK`, then run

    tar czf QtSdk-offline-linux-x86_64-v1.2.1.tar.gz -C /opt QtSDK

Afterwards you can use the commands from the included `.gitlab-ci.yml` file to
build the application.
