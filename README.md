MeeSays
=======

A memory game for [Meego](http://en.wikipedia.org/wiki/MeeGo) (Harmattan)
modeled after the [Simon](http://en.wikipedia.org/wiki/Simon_%28game%29) game.
The official website can be found at
[code.ott.net/meesays](https://code.ott.net/meesays/).

![MeeSays screenshot](https://code.ott.net/meesays/screenshot.png "MeeSays screenshot")


Installing
==========

You can get the source code for MeeSays through git and build it with Qt Creator. Alternatively you can also download precompiled packages right here:

* [meesays_1.0.0_armel.deb for Meego/Harmattan](https://code.ott.net/meesays/downloads/meesays_1.0.0_armel.deb)

If you would like to get automatic updates you can also install the game through the [openrepos package manager](https://openrepos.net/content/cockroach/meesays).


License
=======

This package is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this package. If not, see <http://www.gnu.org/licenses/>.
