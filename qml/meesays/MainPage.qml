import QtQuick 1.1
import com.nokia.meego 1.1
import QtMultimediaKit 1.1

import "game.js" as Game

Page
{
	function initStartPage()
	{
		var high = parseInt(Settings.value("highScore", 0));

		if (high > 0)
		{
			highScore.update(high);
		}
	}

	SoundEffect
	{
		id: soundBlue
		source: "file:///opt/meesays/qml/meesays/blue.wav"
	}

	SoundEffect
	{
		id: soundGreen
		source: "file:///opt/meesays/qml/meesays/green.wav"
	}

	SoundEffect
	{
		id: soundRed
		source: "file:///opt/meesays/qml/meesays/red.wav"
	}

	SoundEffect
	{
		id: soundYellow
		source: "file:///opt/meesays/qml/meesays/yellow.wav"
	}

	Timer
	{
		id: timer
		interval: 200
		running: false
		repeat: true
		onTriggered: Game.game.tick()
	}

	Grid
	{
		id: grid

		columns: 2
		rows: 2

		width: parent.width
		height: parent.height

		ListModel
		{
			id: items
		}

		Rectangle
		{
			id: green

			property color hi: '#00ff00'
			property color low: '#005b00'
			property SoundEffect sound: soundGreen

			color: low

			height: parent.height / 2
			width: parent.width / 2

			Button
			{
				opacity: 0.070
				onClicked: Game.game.push(green)
				anchors.fill: parent
			}
		}

		Rectangle
		{
			id: red

			property color hi: '#ff0000'
			property color low: '#5b0000'
			property SoundEffect sound: soundRed

			color: low

			height: parent.height / 2
			width: parent.width / 2

			Button
			{
				id: redButton
				opacity: 0.070
				onClicked: Game.game.push(red)
				anchors.fill: parent
			}
		}

		Rectangle
		{
			id: yellow

			property color low: '#5b5b00'
			property color hi: '#ffff00'
			property SoundEffect sound: soundYellow

			color: low

			height: parent.height / 2
			width: parent.width / 2

			Button
			{
				opacity: 0.070
				onClicked: Game.game.push(yellow)
				anchors.fill: parent
			}
		}

		Rectangle
		{
			id: blue

			property color hi: '#0000ff'
			property color low: '#00005b'
			property SoundEffect sound: soundBlue

			color: low

			height: parent.height / 2
			width: parent.width / 2

			Button
			{
				opacity: 0.070
				onClicked: Game.game.push(blue)
				anchors.fill: parent
			}
		}
	}

	Rectangle
	{
		id: startlabel
		anchors.centerIn: parent

		Label
		{
			id: start
			text: qsTr("Touch the screen to play")
			anchors.bottom: logo.top
			anchors.bottomMargin: 15
			anchors.horizontalCenter: parent.horizontalCenter

			verticalAlignment: Text.AlignBottom
			color: "#ffffff"

			Component.onCompleted: initStartPage();

			SequentialAnimation on color
			{
				loops: Animation.Infinite

				ColorAnimation
				{
					to: "#999999"
					duration: 2500
				}
				ColorAnimation
				{
					to: "#ffffff"
					duration: 2500
				}
			}
		}

		Label
		{
			function update(val)
			{
				text = "High score: " + val + " point";

				if (val !== 1)
				{
					text += "s";
				}
			}

			id: highScore
			text: qsTr("")
			anchors.top: logo.bottom
			anchors.topMargin: 15
			anchors.horizontalCenter: parent.horizontalCenter
			color: "#999999"
		}

		Image
		{
			id: logo
			source: "meesays.png"
			anchors.centerIn: parent
		}
	}

	QueryDialog
	{
		function updateScore(score)
		{
			var high = Settings.value("highScore", 0);

			message = "";

			if (score > high)
			{
				message += "New high score: ";
				highScore.update(score);
				Settings.setValue("highScore", score);
			}

			message += score + " point";

			if (score !== 1)
			{
				message += "s";
			}
		}

		id: gameover
		titleText: "Game Over"
		acceptButtonText: "Continue"
	}
}
