var Game = function()
{
	var state = new StartState()

	this.push = function(button)
	{
		state = state.push(button)
	}

	this.tick = function()
	{
		state = state.tick()
	}

	this.add = function()
	{
		var buttons = new Array(green, red, yellow, blue)
		var i = Math.floor(Math.random() * 4)
		items.append({'item': buttons[i]})
	}
}

var StartState = function()
{
	this.push = function(button)
	{
		game.add()
		startlabel.visible = false
		return new PlaybackState()
	}
}

var UserPlayState = function()
{
	var step

	this.init = function()
	{
		step = 0
	}

	this.push = function(button)
	{
		button.sound.play()

		var correct = items.get(step).item

		if (button !== correct)
		{
			this.gameOver();
			return new StartState();
		}
		if (++step === items.count)
		{
			game.add()
			timer.start()
		}

		return this
	}

	this.tick = function()
	{
		timer.stop()
		return new PlaybackState()
	}

	this.gameOver = function()
	{
		yellow.sound.play()
		blue.sound.play()

		var score = (items.count - 1);
		gameover.updateScore(score);
		gameover.open()

		startlabel.visible = true

		items.clear()
	}

	this.init()
}

var PlaybackState = function()
{
	var current

	this.init = function()
	{
		current = 0
		timer.start()
	}

	this.push = function(button)
	{
		return this
	}

	this.tick = function()
	{
		var item

		if ((current % 2) === 0)
		{
			item = items.get(current / 2).item

			item.color = item.hi
			item.sound.play()

			current++
			return this
		}
		else
		{
			item = items.get((current -1) / 2).item
			item.color = item.low

			if (++current < (items.count * 2) -1)
				return this

			timer.stop()
			return new UserPlayState()
		}
	}

	this.init()
}

var game = new Game()
