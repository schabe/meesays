#include <QtGui/QApplication>
#include <QDeclarativeContext>

#include "qmlapplicationviewer.h"
#include "settings.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
	QScopedPointer<QApplication> app(createApplication(argc, argv));

	QApplication *d = app.data();
	d->setOrganizationName("cockroach");
	d->setApplicationName("MeeSays");
	Settings *settings = new Settings(d);

	QmlApplicationViewer *viewer = new QmlApplicationViewer();
	viewer->rootContext()->setContextProperty("Settings", settings);
	viewer->setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
	viewer->setMainQmlFile(QLatin1String("qml/meesays/main.qml"));
	viewer->showExpanded();

	return app->exec();
}
